package grupo14.supermercado.adaptadores;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.actividades.Supermercado;
import grupo14.supermercado.fragmentos.ListaProductos;
import grupo14.supermercado.modelos.Categoria;

public class AdaptadorCategorias extends RecyclerView.Adapter<AdaptadorCategorias.CategoriaViewHolder> {

    static class CategoriaViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView nombreCategoria;
        ImageView imagenCategoria;

        CategoriaViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cardview_categoria);
            nombreCategoria = (TextView)itemView.findViewById(R.id.nombre_categoria);
            imagenCategoria = (ImageView)itemView.findViewById(R.id.imagen_categoria);
        }
    }

    private List<Categoria> categorias;
    private Context mContext;

    public AdaptadorCategorias(List<Categoria> categorias, Context ctx) {
        this.categorias = categorias;
        this.mContext = ctx;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public CategoriaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_categoria, viewGroup, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String categoria = (String)((TextView)view.findViewById(R.id.nombre_categoria)).getText();
                ListaProductos productos = ListaProductos.newInstance(categoria);
                cambiarFragmento(productos);
            }
        });
        return new CategoriaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoriaViewHolder categoriaViewHolder, int i) {
        categoriaViewHolder.nombreCategoria.setText(categorias.get(i).getNombre());
        Picasso.with(mContext)
                .load(categorias.get(i).getImagen())
                .fit()
                .centerInside()
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .placeholder(R.drawable.prod_sinimagen)
                .into(categoriaViewHolder.imagenCategoria);
    }

    @Override
    public int getItemCount() {
        return categorias.size();
    }

    private void cambiarFragmento(Fragment fragment) {
        if (mContext == null)
            return;
        if (mContext instanceof Supermercado) {
            Supermercado mainActivity = (Supermercado) mContext;
            mainActivity.cambiarFragmento(fragment, false);
        }

    }

}
