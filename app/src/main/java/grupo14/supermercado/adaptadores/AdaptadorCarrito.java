package grupo14.supermercado.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import grupo14.supermercado.R;
import grupo14.supermercado.modelos.Producto;

public class AdaptadorCarrito extends BaseAdapter {

    private Set<Producto> carrito = new HashSet<>();
    private List<Producto> productos;
    private Context ctx;

    private static class CarritoViewHolder {
        TextView nombre, cantidad, precio;
    }

    public AdaptadorCarrito(Set<Producto> todos, Context ctx) {
        carrito.clear();
        this.carrito.addAll(todos);
        this.ctx = ctx;
        this.productos = new ArrayList<>(new LinkedHashSet<>(carrito));
    }

    public Set<Producto> getCarrito() {
        return carrito;
    }

    public void setCarrito(Set<Producto> carrito) {
        this.carrito = carrito;
    }

    @Override
    public int getCount() {
        return carrito.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CarritoViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(ctx);
            convertView = inflater.inflate(R.layout.lista_carrito, parent, false);
        }

        viewHolder = new CarritoViewHolder();
        viewHolder.nombre = (TextView)convertView.findViewById(R.id.nombre_producto_carrito);
        viewHolder.cantidad = (TextView)convertView.findViewById(R.id.cantidad_producto_carrito);
        viewHolder.precio = (TextView)convertView.findViewById(R.id.precio_producto_carrito);

        viewHolder.nombre.setText(productos.get(position).getNombre());
        viewHolder.cantidad.setText(String.format(Locale.getDefault(), "x%d", productos.get(position).getEnCarrito()));
        viewHolder.precio.setText(ctx.getString(R.string.precio, productos.get(position).getPrecio()));

        return convertView;
    }

}
