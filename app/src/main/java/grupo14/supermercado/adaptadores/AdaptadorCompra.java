package grupo14.supermercado.adaptadores;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.actividades.Supermercado;
import grupo14.supermercado.modelos.Pedido;
import grupo14.supermercado.modelos.Producto;

public class AdaptadorCompra extends RecyclerView.Adapter<AdaptadorCompra.CompraViewHolder> {

    static class CompraViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView fechaCompra;
        TextView precioCompra;
        TextView productosCompra;

        CompraViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cardview_compra);
            fechaCompra = (TextView)itemView.findViewById(R.id.fecha_compra);
            precioCompra = (TextView)itemView.findViewById(R.id.precio_compra);
            productosCompra = (TextView)itemView.findViewById(R.id.productos_compra);
        }
    }

    private List<Pedido> compras;
    private Context ctx;

    public AdaptadorCompra(List<Pedido> compras, Context ctx) {
        this.compras = compras;
        this.ctx = ctx;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        ctx = recyclerView.getContext();
    }

    @Override
    public AdaptadorCompra.CompraViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lista_compra, viewGroup, false);
        return new AdaptadorCompra.CompraViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AdaptadorCompra.CompraViewHolder CompraViewHolder, int i) {
        CompraViewHolder.fechaCompra.setText(compras.get(i).getFecha());
        CompraViewHolder.precioCompra.setText(ctx.getString(R.string.precio, compras.get(i).getTotal()));
        String productos = "";
        for (Producto p: compras.get(i).getProductos()) {
            productos = productos.concat(p.getNombre() + ", ");
        }
        if(productos.length() > 100)
            CompraViewHolder.productosCompra.setText(productos.substring(0, 100).concat("..."));
        else
            CompraViewHolder.productosCompra.setText(productos);

    }

    @Override
    public int getItemCount() {
        return compras.size();
    }

}
