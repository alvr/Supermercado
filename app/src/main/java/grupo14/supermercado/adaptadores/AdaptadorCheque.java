package grupo14.supermercado.adaptadores;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.modelos.Cheque;

public class AdaptadorCheque extends RecyclerView.Adapter<AdaptadorCheque.ChequeViewHolder> {

    private List<Cheque> cheques;
    private Context ctx;

    public AdaptadorCheque(List<Cheque> cheques, Context ctx) {
        this.cheques = cheques;
        this.ctx = ctx;
    }

    @Override
    public ChequeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cheque, parent, false);
        return new AdaptadorCheque.ChequeViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChequeViewHolder holder, int position) {
        holder.caducidadCheque.setText(cheques.get(position).getDesc());
        holder.fechaCheque.setText(cheques.get(position).getFecha());
        holder.precioCheque.setText(ctx.getString(R.string.precio, cheques.get(position).getPrecio()));
    }

    @Override
    public int getItemCount() {
        return cheques.size();
    }

    static class ChequeViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView fechaCheque;
        TextView caducidadCheque;
        TextView precioCheque;

        ChequeViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cardview_cheque);
            fechaCheque = (TextView)itemView.findViewById(R.id.fecha_cheque);
            caducidadCheque = (TextView)itemView.findViewById(R.id.caducidad_cheque);
            precioCheque = (TextView)itemView.findViewById(R.id.precio_cheque);
        }
    }

}
