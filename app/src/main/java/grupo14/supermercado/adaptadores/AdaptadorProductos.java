package grupo14.supermercado.adaptadores;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import grupo14.supermercado.R;
import grupo14.supermercado.fragmentos.Ofertas;
import grupo14.supermercado.modelos.Carrito;
import grupo14.supermercado.modelos.Producto;

public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.ProductoViewHolder> {

    static class ProductoViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView nombreProducto;
        TextView precioProducto;
        TextView descProducto;
        TextView descuentoProducto;
        ImageView imagenProducto;

        ProductoViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cardview_producto);
            nombreProducto = (TextView)itemView.findViewById(R.id.nombre_producto);
            precioProducto = (TextView)itemView.findViewById(R.id.precio_producto);
            descProducto = (TextView)itemView.findViewById(R.id.descripcion_producto);
            imagenProducto = (ImageView)itemView.findViewById(R.id.imagen_producto);
            descuentoProducto = (TextView)itemView.findViewById(R.id.descuento_producto);
        }
    }

    private List<Producto> productos;
    private List<Producto> productosBusqueda = new ArrayList<>();
    private Context ctx;
    private ViewGroup viewgroup;
    private boolean mostrarOfertas;

    public AdaptadorProductos(List<Producto> productos, boolean mostrarOfertas) {
        this.productos = productos;
        productosBusqueda.addAll(productos);
        this.mostrarOfertas = mostrarOfertas;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        ctx = recyclerView.getContext();
    }

    @Override
    public AdaptadorProductos.ProductoViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        viewgroup = viewGroup;
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_producto, viewGroup, false);
        return new AdaptadorProductos.ProductoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AdaptadorProductos.ProductoViewHolder productoViewHolder, int i) {
        productoViewHolder.nombreProducto.setText(productos.get(i).getNombre());
        productoViewHolder.precioProducto.setText(ctx.getString(R.string.precio, productos.get(i).getPrecio()));

        if (mostrarOfertas) {
            productoViewHolder.descuentoProducto.setVisibility(View.VISIBLE);
        } else {
            productoViewHolder.descuentoProducto.setVisibility(View.GONE);
        }
        productoViewHolder.descuentoProducto.setText("⬇ " + productos.get(i).getDescuento() + "%");
        if(productos.get(i).getDescripcion().length() > 100)
            productoViewHolder.descProducto.setText(productos.get(i).getDescripcion().substring(0, 100).concat("..."));
        else
            productoViewHolder.descProducto.setText(productos.get(i).getDescripcion());

        Picasso.with(ctx)
                .load(productos.get(i).getImagen())
                .fit()
                .centerInside()
                .memoryPolicy(MemoryPolicy.NO_STORE)
                .placeholder(R.drawable.prod_sinimagen)
                .into(productoViewHolder.imagenProducto);

        productoViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Producto p = productos.get(productoViewHolder.getAdapterPosition());
                dialogoProducto(p);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public void filter(String filtro) {
        filtro = filtro.toLowerCase(Locale.getDefault());

        productos.clear();
        if (filtro.length() == 0) {
            productos.addAll(productosBusqueda);
        } else {
            for (Producto prod: productosBusqueda) {
                if(filtro.length() != 0) {
                    if (prod.getNombre().toLowerCase(Locale.getDefault()).contains(filtro)) {
                        productos.add(prod);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    private void dialogoProducto(final Producto p) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
        View dialogView = LayoutInflater.from(ctx).inflate(R.layout.popup_producto, viewgroup, false);
        dialog.setView(dialogView);
        dialog.setTitle(p.getNombre());

        ImageView imagen = (ImageView)dialogView.findViewById(R.id.imagen_detalle_producto);
        imagen.setImageResource(p.getImagen());

        TextView desc = (TextView)dialogView.findViewById(R.id.descripcion_detalle_producto);
        desc.setText(p.getDescripcion());

        final NumberPicker cantidad = (NumberPicker)dialogView.findViewById(R.id.cantidad_producto);
        cantidad.setMinValue(1);
        cantidad.setMaxValue(p.getCantidad());

        final TextView precio = (TextView)dialogView.findViewById(R.id.precio_detalle_producto);
        precio.setText((ctx.getString(R.string.detalle_precio, p.getPrecio(), p.getPrecio() * cantidad.getValue())));

        cantidad.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                precio.setText((ctx.getString(R.string.detalle_precio, p.getPrecio(), p.getPrecio() * newVal)));
            }
        });

        dialog.setPositiveButton("Añadir al carrito", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Carrito.INSTANCE.add(p, cantidad.getValue());
                Snackbar.make(viewgroup, "Añadido al carrito: " + p.getNombre(), Snackbar.LENGTH_SHORT).setAction("Deshacer", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Carrito.INSTANCE.remove(p);
                        Snackbar.make(v, "Eliminado del carrito: " + p.getNombre(), Snackbar.LENGTH_SHORT).show();
                    }
                }).show();
            }
        });

        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();
    }
}
