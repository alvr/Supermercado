package grupo14.supermercado.actividades;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import grupo14.supermercado.R;
import grupo14.supermercado.fragmentos.IdeasComer;
import grupo14.supermercado.fragmentos.ListaCarrito;
import grupo14.supermercado.fragmentos.ListaCategorias;
import grupo14.supermercado.fragmentos.ListaProductos;
import grupo14.supermercado.fragmentos.MiCuenta;
import grupo14.supermercado.fragmentos.Novedades;
import grupo14.supermercado.fragmentos.Ofertas;
import grupo14.supermercado.fragmentos.Parking;

public class Supermercado extends AppCompatActivity {

    private Fragment fragment[] = {
            new ListaCategorias(),
            new ListaProductos(),
            new ListaCarrito(),
            new IdeasComer(),
            new MiCuenta(),
            new Novedades(),
            new Ofertas(),
            new Parking()
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.actividad_supermercado);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.nav_abrir, R.string.nav_cerrar);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.nav_categorias:
                        cambiarFragmento(fragment[0], false);
                        break;
                    case R.id.nav_micuenta:
                        cambiarFragmento(fragment[4], false);
                        break;
                    case R.id.nav_novedades:
                        cambiarFragmento(fragment[5], false);
                        break;
                    case R.id.nav_ofertas:
                        cambiarFragmento(fragment[6], false);
                        break;
                    case R.id.nav_ideascomer:
                        cambiarFragmento(fragment[3], false);
                        break;
                }
                drawer.closeDrawers();
                return true;
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cambiarFragmento(fragment[2], true);
            }
        });

        FloatingActionButton parkingFab = (FloatingActionButton)findViewById(R.id.fab_parking);
        parkingFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragmento(fragment[7], false);
            }
        });

        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_principal, fragment[0]).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0) {
            finish();
        }
    }

    public void cambiarFragmento(Fragment fragment, boolean carrito) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (!carrito) {
            ft.setCustomAnimations(R.anim.entrar_derecha, R.anim.salir_izquierda, R.anim.entrar_izquierda, R.anim.salir_derecha);
        } else {
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out);
        }
        ft.replace(R.id.contenido_principal, fragment);
        ft.addToBackStack(fragment.getClass().getSimpleName());
        ft.commit();
    }

}
