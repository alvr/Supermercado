package grupo14.supermercado.actividades;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import grupo14.supermercado.R;
import grupo14.supermercado.fragmentos.Proveedores;

public class IniciarSesion extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_iniciarsesion);

        getSupportFragmentManager().beginTransaction().replace(R.id.contenido_iniciarsesion, new Proveedores()).commit();
    }

}
