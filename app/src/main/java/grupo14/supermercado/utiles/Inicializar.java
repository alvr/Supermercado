package grupo14.supermercado.utiles;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.modelos.Categoria;
import grupo14.supermercado.modelos.Cheque;
import grupo14.supermercado.modelos.Pedido;
import grupo14.supermercado.modelos.Producto;

public class Inicializar {

    private static List<Categoria> categorias = new ArrayList<>();

    public static List<Categoria> initCategorias() {
        categorias.add(new Categoria("Bebé", R.drawable.cat_bebes));
        categorias.add(new Categoria("Bebidas", R.drawable.cat_bebidas));
        categorias.add(new Categoria("Congelados", R.drawable.cat_congelados));
        categorias.add(new Categoria("Droguería y Limpieza", R.drawable.cat_droguerialimpieza));
        categorias.add(new Categoria("Dulces", R.drawable.cat_dulces));
        categorias.add(new Categoria("Mascotas", R.drawable.cat_mascotas));
        categorias.add(new Categoria("Lácteos", R.drawable.cat_lacteos));
        categorias.add(new Categoria("Pan, Bollería y Galletas", R.drawable.cat_pan));
        categorias.add(new Categoria("Productos frescos", R.drawable.cat_productosfrescos));

        return categorias;
    }

    public static List<Producto> initProductos(String categoria) {
        switch (categoria) {
            case "Bebé":
                return initProductosBebe();
            case "Bebidas":
                return initProductosBebida();
            case "Congelados":
                return initProductosCongelado();
            case "Droguería y Limpieza":
                return initProductosDrogueria();
            case "Dulces":
                return initProductosDulces();
            case "Mascotas":
                return initProductosMascotas();
            case "Lácteos":
                return initProductosLacteos();
            case "Pan, Bollería y Galletas":
                return initProductosPan();
            case "Productos frescos":
                return initProductosFrescos();
            default:
                return new ArrayList<Producto>() {{
                    add(new Producto("Sin información", "Sin información.", R.drawable.prod_sinimagen, 5, 999.99));
                }};
        }
    }

    private static List<Producto> initProductosBebe() {
        return new ArrayList<Producto>() {{
            add(new Producto("Pañales", "Ofrece máxima absorción y sequedad para el cuidado de la piel delicada.", R.drawable.prod_panales, 5, 9.99));
            add(new Producto("Biberones", "Tetina con una forma única de fácil agarre. Tetina de silicona blanda y " +
                    "de tacto natural. Fácil de sujetar y con tetina de flujo lento.", R.drawable.prod_biberones, 7, 4.99));
            add(new Producto("Carrito", "Una celebración contemporánea de un clásico: la Quest trae soplo de aire " +
                    "fresco al paseo con nuevos colores. El Sistema de Seguridad para Recién Nacido y Capazo permiten " +
                    "a la Quest poderse adaptar a cualquier viaje. Con un asiento cómodamente acolchado y una suspensión " +
                    "de muelle, la Quest asegura el más suave, más seguro y más acogedor paseo.", R.drawable.prod_carrito, 2, 149.99));
            add(new Producto("Silla de coche", "Milofix es una Silla de Auto Isofix para el grupo 0+ y 1 que permite " +
                    "prolongar el uso de espaldas a la marcha hasta los 15 meses de edad. Isofix significa máxima seguridad " +
                    "y fácil instalación con un sólo clic. Perfecto para los papás muy activos.", R.drawable.prod_carrito_coche, 3, 200));
            add(new Producto("Nativa ProExcel 1", "Leche infantil adecuada para la alimentación del lactante " +
                    "sano desde el primer día cuando la lactancia materna no es posible", R.drawable.prod_leche_bebe, 20, 8.25));
            add(new Producto("Gimnasio-piano", "Innovador gimnasio que incorpora un piano " +
                    "que el bebé puede tocar desde 3 posiciones diferentes", R.drawable.prod_bebe_juguete, 6, 60.30));
        }};
    }

    private static List<Producto> initProductosBebida() {
        return new ArrayList<Producto>() {{
            add(new Producto("Cocacola", "Pack de 6 latas de Cola-Cola.", R.drawable.prod_cocacola, 25, 9.99));
            add(new Producto("Fanta", "Pack de 6 latas de Fanta naranja.", R.drawable.prod_fanta, 27, 8.99));
            add(new Producto("Agua Mineral", "Botella de agua mineral Lanjarón 50 cl", R.drawable.prod_agua_mineral, 20, 0.70));
            add(new Producto("Mountain Dew", "Energy. Refresco de lima limon botella 50 cl", R.drawable.prod_mountain_dew, 20, 0.80));
            add(new Producto("Sunny Delight", "Florida. Refresco multifrutas envase de 1,25 l", R.drawable.prod_sunny, 20, 1.59));
        }};
    }

    private static List<Producto> initProductosCongelado() {
        return new ArrayList<Producto>() {{
            add(new Producto("Helado Haggen-Dazs", "Helado de vainilla. Tarrina de 500 ml", R.drawable.prod_helado, 20, 5.99));
            add(new Producto("Pizza Buitoni", "Forno di Pietra barbacoa y bacon. Estuche de 325 g", R.drawable.prod_pizza, 20, 3.19));
            add(new Producto("Merluza Pescanova", "Porciones de merluza congelada. 400 g", R.drawable.prod_merluza, 20, 3.70));
            add(new Producto("Croquetas Cocinera", "Sabrosisimas croquetas de jamon serrano. Bolsa de 500 g", R.drawable.prod_croquetas, 20, 2.59));
            add(new Producto("Verduras Findus", "Findus Salto Salteado Campestre. Bolsa de 500 g", R.drawable.prod_salto, 20, 3.27));
        }};
    }

    private static List<Producto> initProductosDrogueria() {
        return new ArrayList<Producto>() {{
            add(new Producto("Pasta Colgate", "Pasta de Dientes Colgate Total. Pack de 3 75 ml", R.drawable.prod_pasta_dientes, 20, 5.05));
            add(new Producto("Champu Pantene", "Pantene Pro-V. Champu + Acondicionador. Frasco de 700 ml", R.drawable.prod_champu, 20, 6.15));
            add(new Producto("Desodorante Nivea", "Desodorante Nivea Invisible Fresh. Spray de 200 ml", R.drawable.prod_desodorante, 20, 3.20));
            add(new Producto("Cuchillas Gillette", "Recambio de Maquinilla de Afeitas. Estuche de 8 unidades", R.drawable.prod_gillette, 20, 28.15));
            add(new Producto("Perfume OneMillion", "Perfume Paco Rabanne. Frasco de 100 ml", R.drawable.prod_colonia, 20, 60.25));
        }};
    }

    private static List<Producto> initProductosMascotas() {
        return new ArrayList<Producto>() {{
            add(new Producto("Comida para Perros", "Ultima Senior. Alimento completo para perros +7 años. Bolsa 7,5 kg", R.drawable.prod_comida_perro, 20, 26.45));
            add(new Producto("Correa Flexi", "Correa para Perros. Color rosa. Talla S. 1 Unidad", R.drawable.prod_correa_perros, 20, 16.19));
            add(new Producto("Comida para Gatos", "Ultima con Salmón y Cebada. Bolsa de 3 kg", R.drawable.prod_comida_gato, 20, 13.95));
            add(new Producto("Comida Loros Nido", "Friskies Nido. Alimento completo para Loros. Estuche de 400 g", R.drawable.prod_comida_loros, 20, 13.95));
            add(new Producto("Comedero Biozoo", "Biozoo Axis. Comedero de acero inoxidable. Tamaño pequeño 0,4. 1 Unidad", R.drawable.prod_comedero_perros, 20, 3));

        }};
    }

    private static List<Producto> initProductosLacteos() {
        return new ArrayList<Producto>() {{
            add(new Producto("LecheEntera Pascual", "Leche Entera. Envase 1 l", R.drawable.prod_leche, 20, 0.89));
            add(new Producto("Mantequilla", "Mantequilla Mediterranea con Aceite de Oliva. Tarrina de 250 g", R.drawable.prod_mantequilla, 20, 2));
            add(new Producto("Nata para Montar", "Nata Liquida para Montar. 35% MG. Envase de 500 ml", R.drawable.prod_nata, 20, 1.76));
            add(new Producto("Batido Cola Cao", "Cola Cao Shake. Leche y cacao natural. Envase de 200 ml", R.drawable.prod_batido, 20, 1.20));
            add(new Producto("Vive Soy", "Bebida de Soja Sabor Natural. Envase 1 l", R.drawable.prod_soja, 20, 1.48));
        }};
    }

    private static List<Producto> initProductosFrescos() {
        return new ArrayList<Producto>() {{
            add(new Producto("Escalopin de Cerdo", "Elpozo extratiernos. Lomo fresco. Bandeja de 600 g", R.drawable.prod_escalopin, 20, 3.95));
            add(new Producto("Ternera Filetes", "Filetes de Lomo Alto. Bandeja de 400 g Aprox", R.drawable.prod_ternera, 20, 9.18));
            add(new Producto("Chorizo Navidul", "Surtido Gourmet Chorizo y Salchichon Ibericos. Envase de 110 g", R.drawable.prod_chorizo, 20, 2.04));
            add(new Producto("Queso Garcia Vaquero", "Queso Semicurado en Lonchas. Bandeja de 200 g", R.drawable.prod_queso, 20, 2.86));
            add(new Producto("Jamon Serrano", "Jamon Curado en Medias Lonchas. Envase de 170 g", R.drawable.prod_jamon_serrano, 20, 3.16));
        }};
    }

    private static List<Producto> initProductosDulces() {
        return new ArrayList<Producto>() {{
            add(new Producto("Kit Kat", "Pack 3 envases x 40 g", R.drawable.prod_kitkat, 20, 1.97));
            add(new Producto("Caramelos Smint", "Caramelos Smint Sin Azucar. Sabor Polar. 3 x 50 unidades", R.drawable.prod_smint, 20, 2.99));
            add(new Producto("Caja Roja Nestle", "Bombones Surtidos. Estuche de 800 g", R.drawable.prod_bombones, 20, 16.79));
            add(new Producto("Chicles Trident", "Chicles de Fresa Stick Sin Azucar. Envase de 5 Unidades", R.drawable.prod_chicles, 20, 1.95));
            add(new Producto("Conguitos", "Original. Cacahuetes Recubiertos de Chocolate con Leche. Bolsa de 240 g", R.drawable.prod_conguitos, 20, 2.01));
        }};
    }

    private static List<Producto> initProductosPan() {
        return new ArrayList<Producto>() {{
            add(new Producto("Pan Tostado", "Pan Tostado Recondo Hogaza. 30 Rebanadas. Paquete de 270 g", R.drawable.prod_pan_tostado, 20, 1.55));
            add(new Producto("Pan Molde Bimbo", "Pan de Molde con Corteza Grande. Bolsa de 420 g", R.drawable.prod_pan_molde, 20, 2.08));
            add(new Producto("Magdalenas Bella", "Magdalenas Estilo Artesanas. Bolsa de 360 g", R.drawable.prod_magdalenas, 20, 1.75));
            add(new Producto("Tosta Rica", "Galletas de Desayuno con Vitaminas Hierro Calcio. Caja de 860 g", R.drawable.prod_galletas, 20, 2.89));
            add(new Producto("Pastel Queso", "Pastel de Queso para Preparar Royal. Estuche de 325 g", R.drawable.prod_tarta, 20, 2.81));
        }};
    }

    public static List<Pedido> pedidos() {
        return new ArrayList<Pedido>() {{
            add(new Pedido(new HashSet<Producto>() {{
                add(new Producto("Kit Kat", "Pack 3 envases x 40 g", R.drawable.prod_kitkat, 20, 1.97));
                add(new Producto("Chicles Trident", "Chicles de Fresa Stick Sin Azucar. Envase de 5 Unidades", R.drawable.prod_chicles, 20, 1.95));
                add(new Producto("Queso Garcia Vaquero", "Queso Semicurado en Lonchas. Bandeja de 200 g", R.drawable.prod_queso, 20, 2.86));
                add(new Producto("Nata para Montar", "Nata Liquida para Montar. 35% MG. Envase de 500 ml", R.drawable.prod_nata, 20, 1.76));
                add(new Producto("Comida Loros Nido", "Friskies Nido. Alimento completo para Loros. Estuche de 400 g", R.drawable.prod_comida_loros, 20, 13.95));
            }}, 22.49, "07/04/2017"));

            add(new Pedido(new HashSet<Producto>() {{
                add(new Producto("Escalopin de Cerdo", "Elpozo extratiernos. Lomo fresco. Bandeja de 600 g", R.drawable.prod_escalopin, 20, 3.95));
                add(new Producto("Jamon Serrano", "Jamon Curado en Medias Lonchas. Envase de 170 g", R.drawable.prod_jamon_serrano, 20, 3.16));
                add(new Producto("Batido Cola Cao", "Cola Cao Shake. Leche y cacao natural. Envase de 200 ml", R.drawable.prod_batido, 20, 1.20));
            }}, 8.31, "12/04/2017"));

            add(new Pedido(new HashSet<Producto>() {{
                add(new Producto("Pastel Queso", "Pastel de Queso para Preparar Royal. Estuche de 325 g", R.drawable.prod_tarta, 20, 2.81));
                add(new Producto("Pasta Colgate", "Pasta de Dientes Colgate Total. Pack de 3 75 ml", R.drawable.prod_pasta_dientes, 20, 5.05));
                add(new Producto("Helado Haggen-Dazs", "Helado de vainilla. Tarrina de 500 ml", R.drawable.prod_helado, 20, 5.99));
                add(new Producto("Champu Pantene", "Pantene Pro-V. Champu + Acondicionador. Frasco de 700 ml", R.drawable.prod_champu, 20, 6.15));
                add(new Producto("Pan Tostado", "Pan Tostado Recondo Hogaza. 30 Rebanadas. Paquete de 270 g", R.drawable.prod_pan_tostado, 20, 1.55));
                add(new Producto("Nativa ProExcel 1", "Leche infantil adecuada para la alimentación del lactante " +
                        "sano desde el primer día cuando la lactancia materna no es posible", R.drawable.prod_leche_bebe, 20, 8.25));
            }}, 29.8, "17/04/2017"));
        }};
    }

    public static List<Cheque> cheques() {
        return new ArrayList<Cheque>() {{
            add(new Cheque(10, "25/07/2017", "Quedan XX días para que expire."));
            add(new Cheque(5, "20/04/2017", "Quedan XX días para que expire"));
        }};
    }

}
