package grupo14.supermercado.fragmentos;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import grupo14.supermercado.R;
import grupo14.supermercado.actividades.IniciarSesion;
import grupo14.supermercado.actividades.Supermercado;

public class MiCuenta extends Fragment {

    FloatingActionButton fab;
    FloatingActionButton counterFab;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().setTitle("Mi Cuenta");
        return inflater.inflate(R.layout.fragmento_micuenta, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        counterFab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        counterFab.setVisibility(View.INVISIBLE);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_parking);
        fab.setVisibility(View.INVISIBLE);

        TextView hola = (TextView)getActivity().findViewById(R.id.bienvenido);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            hola.setText(getString(R.string.bienvenido, FirebaseAuth.getInstance().getCurrentUser().getDisplayName()));
        } else {
            hola.setVisibility(View.GONE);
        }

        Button datosPersonales = (Button)getActivity().findViewById(R.id.datos_personales);
        datosPersonales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                final AlertDialog show = alertDialog.show();
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                alertDialog
                        .setTitle("Datos personales")
                        .setMessage("Nombre: " + user.getDisplayName() + "\n" + "Correo: " + user.getEmail())
                        .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                show.dismiss();
                            }
                        })
                        .show();
            }
        });

        Button editarPerfil = (Button)getActivity().findViewById(R.id.editar_perfil);
        editarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragmento(new EditarPerfil());
            }
        });

        Button compras = (Button)getActivity().findViewById(R.id.ver_compras);
        compras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragmento(new Compras());
            }
        });

        Button cheques = (Button)getActivity().findViewById(R.id.cheques_regalo);
        cheques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragmento(new Cheques());
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.micuenta, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cerrar_sesion:
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                final AlertDialog show = alertDialog.show();

                alertDialog
                        .setTitle("¿Cerrar sesión?")
                        .setMessage("¿Cerrar sesión y volver a la pantalla de inicio?")
                        .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseAuth.getInstance().signOut();
                                Intent intent = new Intent(getContext(), IniciarSesion.class);
                                startActivityForResult(intent, 0);
                            }
                        })
                        .setNegativeButton("Cerrar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                show.dismiss();
                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fab.setVisibility(View.VISIBLE);
        counterFab.setVisibility(View.VISIBLE);
    }

    private void cambiarFragmento(Fragment fragment) {
        if (getContext() == null)
            return;
        if (getContext() instanceof Supermercado) {
            Supermercado mainActivity = (Supermercado) getContext();
            mainActivity.cambiarFragmento(fragment, false);
        }

    }
}
