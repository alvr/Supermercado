package grupo14.supermercado.fragmentos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.adaptadores.AdaptadorCompra;
import grupo14.supermercado.modelos.Pedido;
import grupo14.supermercado.utiles.Inicializar;

public class Compras extends Fragment {

    AdaptadorCompra adaptador;
    List<Pedido> compras;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        compras = Inicializar.pedidos();

        adaptador = new AdaptadorCompra(compras, getContext());
        getActivity().setTitle("Compras");
        return inflater.inflate(R.layout.fragmento_contenido, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton counterFab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        counterFab.setVisibility(View.INVISIBLE);
        FloatingActionButton fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_parking);
        fab.setVisibility(View.INVISIBLE);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.contenido);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        rv.setAdapter(adaptador);
    }

}
