package grupo14.supermercado.fragmentos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.adaptadores.AdaptadorCheque;
import grupo14.supermercado.modelos.Cheque;
import grupo14.supermercado.utiles.Inicializar;

public class Cheques extends Fragment {

    AdaptadorCheque adaptador;
    FloatingActionButton fab;
    FloatingActionButton counterFab;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        List<Cheque> cheques = Inicializar.cheques();
        adaptador = new AdaptadorCheque(cheques, getContext());
        getActivity().setTitle("Mis cheques");
        return inflater.inflate(R.layout.fragmento_contenido, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        counterFab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        counterFab.setVisibility(View.INVISIBLE);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_parking);
        fab.setVisibility(View.INVISIBLE);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.contenido);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        rv.setAdapter(adaptador);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fab.setVisibility(View.VISIBLE);
        counterFab.setVisibility(View.VISIBLE);
    }

}
