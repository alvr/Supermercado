package grupo14.supermercado.fragmentos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import grupo14.supermercado.R;
import grupo14.supermercado.adaptadores.AdaptadorProductos;
import grupo14.supermercado.modelos.Categoria;
import grupo14.supermercado.modelos.Producto;
import grupo14.supermercado.utiles.Inicializar;

public class Ofertas extends Fragment {

    List<Categoria> categorias;
    List<Producto> productos;
    AdaptadorProductos adaptador;
    final List<Integer> descuentos = new ArrayList<Integer>() {{
        add(5);
        add(10);
        add(20);
        add(30);
        add(40);
        add(50);
        add(60);
        add(70);
    }};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        categorias = new ArrayList<>();
        productos = new ArrayList<>();

        List<Categoria> cat = Inicializar.initCategorias();
        Collections.shuffle(cat);

        categorias = cat.subList(0, 3);

        for (Categoria c : categorias) {
            List<Producto> prd = Inicializar.initProductos(c.getNombre());
            Collections.shuffle(prd);

            prd = prd.subList(0, 3);

            for(Producto p : prd) {
                Collections.shuffle(descuentos);
                p.setDescuento(descuentos.get(0));
                p.setPrecio(p.getPrecio() - p.getPrecio() * descuentos.get(0)/100);
            }

            productos.addAll(prd);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        adaptador = new AdaptadorProductos(productos, true);
        getActivity().setTitle("Ofertas");
        return inflater.inflate(R.layout.fragmento_contenido, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        RecyclerView rv = (RecyclerView)view.findViewById(R.id.contenido);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(llm);
        rv.setAdapter(adaptador);
    }

}
