package grupo14.supermercado.fragmentos;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import grupo14.supermercado.R;

public class IdeasComer extends Fragment {

    FloatingActionButton fab;
    FloatingActionButton counterFab;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Ideas para Comer");
        return inflater.inflate(R.layout.fragmento_ideascomer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        counterFab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        counterFab.setVisibility(View.INVISIBLE);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_parking);
        fab.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fab.setVisibility(View.VISIBLE);
        counterFab.setVisibility(View.VISIBLE);
    }
}
