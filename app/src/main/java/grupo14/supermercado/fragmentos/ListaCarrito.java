package grupo14.supermercado.fragmentos;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import grupo14.supermercado.R;
import grupo14.supermercado.adaptadores.AdaptadorCarrito;
import grupo14.supermercado.modelos.Carrito;
import grupo14.supermercado.modelos.Categoria;
import grupo14.supermercado.modelos.Pedido;
import grupo14.supermercado.modelos.Producto;

public class ListaCarrito extends Fragment {

    AdaptadorCarrito adaptador;
    FloatingActionButton fab;
    FloatingActionButton counterFab;
    FirebaseDatabase mDatabase;
    Set<Producto> carrito = new HashSet<>();
    ListView lista;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmento_carrito, container, false);

        carrito.clear();
        carrito.addAll(Carrito.INSTANCE.productos());
        mDatabase = FirebaseDatabase.getInstance();
        getActivity().setTitle("Carrito: " + carrito.size());

        lista = (ListView)view.findViewById(R.id.lista_carrito);

        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final String nombre = (String) ((TextView)view.findViewById(R.id.nombre_producto_carrito)).getText();
                Carrito.INSTANCE.remove(nombre);
                adaptador.setCarrito(Carrito.INSTANCE.productos());
                adaptador.notifyDataSetChanged();
                adaptador.notifyDataSetInvalidated();
                TextView precio = (TextView)getActivity().findViewById(R.id.precio_total);
                precio.setText(Carrito.INSTANCE.precio() + "€");
                Button pagar = (Button)getActivity().findViewById(R.id.boton_pagar);
                getActivity().setTitle("Carrito: " + carrito.size());
                if(adaptador.getCarrito().size() < 1) {
                    pagar.setEnabled(false);
                } else {
                    pagar.setEnabled(true);
                }
                Toast.makeText(getContext(), "Producto quitado del carrito: " + nombre, Toast.LENGTH_LONG).show();
                return true;
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adaptador = new AdaptadorCarrito(carrito, getContext());

        if(lista.getAdapter() == null)
            lista.setAdapter(adaptador);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        counterFab = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        counterFab.setVisibility(View.INVISIBLE);
        fab = (FloatingActionButton)getActivity().findViewById(R.id.fab_parking);
        fab.setVisibility(View.INVISIBLE);

        TextView precio = (TextView)getActivity().findViewById(R.id.precio_total);
        precio.setText(Carrito.INSTANCE.precio() + "€");

        Button pagar = (Button)view.findViewById(R.id.boton_pagar);
        if(carrito.size() < 1) {
            pagar.setEnabled(false);
        } else {
            pagar.setEnabled(true);
        }

        pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DialogoPago(mDatabase.getReference("pedidos")).crear().show();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vaciar_carrito, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.vaciar_carrito:
                Carrito.INSTANCE.clear();
                adaptador.getCarrito().clear();
                adaptador.notifyDataSetChanged();
                adaptador.notifyDataSetInvalidated();
                TextView precio = (TextView)getActivity().findViewById(R.id.precio_total);
                precio.setText(Carrito.INSTANCE.precio() + "€");
                getActivity().setTitle("Carrito: " + carrito.size());
                Button pagar = (Button)getActivity().findViewById(R.id.boton_pagar);
                if(adaptador.getCarrito().size() < 1) {
                    pagar.setEnabled(false);
                } else {
                    pagar.setEnabled(true);
                }
                Toast.makeText(getContext(), "Carrito vaciado", Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        fab.setVisibility(View.VISIBLE);
        counterFab.setVisibility(View.VISIBLE);
    }

    private class DialogoPago {

        DatabaseReference mRef;
        ListAdapter adapter;
        Context ctx;

        class Item {
            final String text;
            final int icon;

            Item(String text, Integer icon) {
                this.text = text;
                this.icon = icon;
            }

            @Override
            public String toString() {
                return text;
            }
        }

        final Item[] items = {
            new Item("Efectivo", R.drawable.ic_pagar_efectivo),
            new Item("Tarjeta de crédito", R.drawable.ic_pagar_tarjeta),
            new Item("PayPal", R.drawable.ic_pagar_paypal)
        };

        DialogoPago(DatabaseReference mRef) {
            this.mRef = mRef;
            ctx = ListaCarrito.this.getContext();
            adapter = new ArrayAdapter<Item>(ctx, android.R.layout.select_dialog_item, android.R.id.text1, items) {
                @NonNull
                public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    TextView tv = (TextView)v.findViewById(android.R.id.text1);
                    tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);
                    int dp5 = (int) (5 * ListaCarrito.this.getResources().getDisplayMetrics().density + 0.5f);
                    tv.setCompoundDrawablePadding(dp5);

                    return v;
                }
            };
        }

        AlertDialog.Builder crear() {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
            final AlertDialog show = alertDialog.show();

            return alertDialog
                    .setTitle("Selecciona método de pago")
                    .setAdapter(adapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String userId = mRef.push().getKey();
                            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", Locale.getDefault());
                            Pedido p = new Pedido(carrito, Carrito.INSTANCE.precio(), dateFormat.format(new Date()));
                            mRef.child(userId).setValue(p);
                            Toast.makeText(ctx, "Pagado con " + items[which].text, Toast.LENGTH_LONG).show();
                            Carrito.INSTANCE.clear();
                            show.dismiss();
                            ListaCarrito.this.getFragmentManager().popBackStackImmediate();
                        }
                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            show.dismiss();
                        }
                    });
        }
    }


}
