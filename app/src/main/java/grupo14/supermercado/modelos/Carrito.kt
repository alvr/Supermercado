package grupo14.supermercado.modelos

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.ArrayList
import java.util.HashSet

object Carrito {
    private val listaProductos = ArrayList<Producto>()

    fun add(p: Producto, cantidad: Int) {
        val lista = ArrayList<String>()

        listaProductos.forEach {
            lista.add(it.nombre)
        }

        if(lista.contains(p.nombre)) {
            var count = 0
            val it = listaProductos.iterator()
            while (it.hasNext()) {
                val pr = it.next()

                if (pr.nombre == p.nombre) {
                    count = pr.enCarrito
                    it.remove()
                }
            }
            p.enCarrito = count + cantidad
            listaProductos.add(p)
        } else {
            p.enCarrito = cantidad
            listaProductos.add(p)
        }
    }

    fun remove(p: Producto): Boolean {
        return listaProductos.remove(p)
    }

    fun remove(n: String) {
        val lista = ArrayList<String>()

        listaProductos.forEach {
            lista.add(it.nombre)
        }

        if(lista.contains(n)) {
            val elem = lista.indexOf(n)
            listaProductos.removeAt(elem)
        }
    }

    fun precio(): Double {
        val precioTotal = listaProductos.sumByDouble { it.precio * it.enCarrito }

        var bd = BigDecimal(precioTotal)
        bd = bd.setScale(2, RoundingMode.HALF_UP)
        return bd.toDouble()
    }

    fun clear() {
        listaProductos.clear()
    }

    fun productos(): Set<Producto> {
        return HashSet<Producto>(listaProductos)
    }

}
