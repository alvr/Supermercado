package grupo14.supermercado.modelos;

import com.google.firebase.database.Exclude;

public class Producto {

    private String nombre;
    private String descripcion;
    private int imagen;
    private int cantidad;
    private double precio;
    private int descuento;

    private int enCarrito = 0;

    public Producto(String nombre, String descripcion, int imagen, int cantidad, double precio) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    @Exclude
    public String getDescripcion() {
        return descripcion;
    }

    @Exclude
    public int getImagen() {
        return imagen;
    }

    @Exclude
    public int getCantidad() {
        return cantidad;
    }

    public int getEnCarrito() {
        return enCarrito;
    }

    public void setEnCarrito(int enCarrito) {
        this.enCarrito = enCarrito;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

}
