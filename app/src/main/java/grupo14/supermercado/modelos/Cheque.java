package grupo14.supermercado.modelos;

public class Cheque {

    private double precio;
    private String fecha;
    private String desc;

    public Cheque(double precio, String fecha, String desc) {
        this.precio = precio;
        this.fecha = fecha;
        this.desc = desc;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
