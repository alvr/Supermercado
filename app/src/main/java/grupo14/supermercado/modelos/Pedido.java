package grupo14.supermercado.modelos;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Pedido {

    private List<Producto> productos;
    private final double total;
    private final String fecha;

    public Pedido(Set<Producto> productos, double total, String fecha) {
        this.productos = new ArrayList<>(new LinkedHashSet<>(productos));
        this.total = total;
        this.fecha = fecha;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public double getTotal() {
        return total;
    }

    public String getFecha() {
        return fecha;
    }
}
